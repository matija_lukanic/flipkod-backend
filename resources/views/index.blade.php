<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Flipkod calc</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    </head>
    <body>
        <div class="jumbotron jumbotron-fluid mb-0">

            <form method="post" id="sum-sc">
                {{ csrf_field() }}

              <div class="container">

                <div class="d-flex justify-content-around flex-sm-row flex-column">
                      
                    <div class="card align-self-start col-xs-12 col-md-4 mb-4">
                      <div class="card-body">

                        <h5 class="card-title mb-4">Circle</h5>
                        <p class="card-text">Please enter the circle's radius to get the surface and circumference.</p>
        
                        <div class="input-group mb-5">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon3">Radius</span>
                          </div>
                          <input type="number" class="form-control" id="circle-radius" name="circleRadius" aria-describedby="basic-addon3" value="{{!empty($circle) ? $circle->radius : ''}}">
                        </div>

                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon3">Surface</span>
                          </div>
                          <input type="number" class="form-control" id="circle-surface" aria-describedby="basic-addon3" disabled value="{{!empty($circle) ? $circle->surface : ''}}">
                        </div>

                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon3">Circumference</span>
                          </div>
                          <input type="number" class="form-control" id="circle-circumference" aria-describedby="basic-addon3" disabled value="{{!empty($circle) ? $circle->circumference : ''}}">
                        </div>

                      </div>
                    </div>

                    <div class="card align-self-start col-xs-12 col-md-4">
                      <div class="card-body">
                        <h5 class="card-title mb-4">Triangle</h5>
                        <p class="card-text">Please enter the triangle's A, B and C sides to get the surface and circumference.</p>

                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon3">A</span>
                          </div>
                          <input type="number" class="form-control triangle-input" id="triangle-a" name="triangleA" aria-describedby="basic-addon3" value="{{!empty($triangle) ? $triangle->a : ''}}">
                        </div>

                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon3">B</span>
                          </div>
                          <input type="number" class="form-control triangle-input" id="triangle-b" name="triangleB" aria-describedby="basic-addon3" value="{{!empty($triangle) ? $triangle->b : ''}}">
                        </div>

                        <div class="input-group mb-5">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon3">C</span>
                          </div>
                          <input type="number" class="form-control triangle-input" id="triangle-c" name="triangleC" aria-describedby="basic-addon3" value="{{!empty($triangle) ? $triangle->c : ''}}">
                        </div>


                        <p class="card-text text-danger triangle-warning d-none">This is not a valid triangle.</p>

                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon3">Surface</span>
                          </div>
                          <input type="number" class="form-control" id="triangle-surface" aria-describedby="basic-addon3" disabled value="{{!empty($triangle) ? $triangle->surface : ''}}">
                        </div>

                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon3">Circumference</span>
                          </div>
                          <input type="number" class="form-control" id="triangle-circumference" aria-describedby="basic-addon3" disabled value="{{!empty($triangle) ? $triangle->circumference : ''}}">
                        </div>

                      </div>
                    </div>

                </div>

              </div>

            </form>

          @if( !empty($sumSurfaces) && !empty($sumCircumference) )

            <div id="sc-div" class="container mt-5">
                <div class="d-flex justify-content-center">
                    <div class="text-center col-xs-12 col-md-8 bg-white b-1 p-4">
                        <h4 class="mb-3">
                            Circle surface + Triangle surface = {{ $sumSurfaces }}
                        </h4>
                        <h4>
                            Circle circumference + Triangle circumference = {{ $sumCircumference }}
                        </h4>
                    </div>
                </div>
            </div>

          @endif

          <div class="container">
              <div class="text-center mt-5">
                <button id="btn-submit" type="button" class="btn btn-primary btn-lg {{(!empty($triangle) && !empty($circle)) ? '' : 'disabled'}}">Sum surfaces and circumferences of the circle and triangle</button>
              </div>
          </div>

        <!-- jumbotron end -->
        </div>

        <script src= "{{ URL::asset('js/custom.js') }}" ></script>
    </body>
</html>
