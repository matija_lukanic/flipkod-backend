$(document).ready(function(){

	//when user changes radius value of the circle input
	$( "#circle-radius" ) .on('input', function () {    

		//remove div that shows surface and circumference sums
		$('#sc-div').remove();

		//check if circle radius value is a number
		if( $.isNumeric($(this).val()) )
		{
		    $.get( "circle/" + $(this).val(), {
		      
		    }, function(circle) {
		        
		        //get circle json and fill surface and circumference input
		    	$('#circle-surface').val(circle.surface);
		    	$('#circle-circumference').val(circle.circumference);

		    	//if all three sides are numbers then enable submit btn
				triangleSidesAreNumbers() ? enableSubmitBtn() : '';

		    });
		}
		else
		{
			//circle radius is not a number, empty surface and circumference and disable submit button
		    $('#circle-surface').val('');
		    $('#circle-circumference').val('');
		    disableSubmitBtn();
		}


	});  
	

	//when user changes any of the A B C input values of the triangle
	$( ".triangle-input" ) .on('input', function () {    

		//remove div that shows surface and circumference sums
		$('#sc-div').remove();

		//if all three sides are numbers then get triangle json
		if( triangleSidesAreNumbers() )
		{ 
			//get triangle sides value
			var a = $('#triangle-a').val();
			var b = $('#triangle-b').val();
			var c = $('#triangle-c').val();

		    $.get( "triangle/" + a + '/' + b + '/' + c, {
		      
		    }, function(triangle) {
		        
		        //get triangle json and check if it is an object

		        if( typeof triangle == 'object')
		        {	
		        	//triangle is json, fill surface and circumference input,
		        	//and hide warning text

		        	$('.triangle-warning').addClass('d-none'); 
		    		$('#triangle-surface').val(triangle.surface);
		    		$('#triangle-circumference').val(triangle.circumference);

		    		//if circle surface is calculated enable submit button
		    		$.isNumeric($('#circle-surface').val()) ? enableSubmitBtn() : '';

		        }
		        else
		        {		     
		        	//triangle is not a valid triangle, empty surface and circumference input,
		        	//show warning text and disable submit button

		        	$('.triangle-warning').removeClass('d-none'); 
		    		$('#triangle-surface').val('');
		    		$('#triangle-circumference').val('');
		    		disableSubmitBtn();
		        }		        
		    });
		}
		else
		{	
			//triangle sides are not number, empty surface and circumference and disable button
		    		
	   		$('#triangle-surface').val('');
	   		$('#triangle-circumference').val('');
			disableSubmitBtn();
		}

	}); 


	//button for submiting form for calculating sum of surface and circumference
	$('#btn-submit').click(function(){

		//if button is not disabled then submit form
		if(!$(this).hasClass('disabled'))
		{
			$('#sum-sc').submit();
		}

	});

	function triangleSidesAreNumbers()
	{
		//check if all triangle sides are numbers

		var a = $('#triangle-a').val();
		var b = $('#triangle-b').val();
		var c = $('#triangle-c').val();

		if( $.isNumeric(a) && $.isNumeric(b) && $.isNumeric(c))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function enableSubmitBtn()
	{
		$('#btn-submit').removeClass('disabled');
	}

	function disableSubmitBtn()
	{
		$('#btn-submit').addClass('disabled');
	}

});