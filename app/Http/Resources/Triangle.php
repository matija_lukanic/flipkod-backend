<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Triangle extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => $this->type,
            'a' => $this->a,
            'b' => $this->b,
            'c' => $this->c,
            'surface' => round($this->surface, 2),
            'circumference' => $this->circumference
        ];
    }
}
