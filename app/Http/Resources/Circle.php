<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Circle extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => $this->type,
            'radius' => $this->radius,
            'surface' => round($this->surface, 2),
            'circumference' => round($this->circumference, 2)
        ];
    }
}
