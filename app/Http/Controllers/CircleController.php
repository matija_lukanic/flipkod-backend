<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\Circle as CircleResource;
use App\Circle;

class CircleController extends Controller
{
    public function json($radius)
    {    	
	    $circle = new Circle($radius);

	    return response()->json(new CircleResource($circle));
    }
}
