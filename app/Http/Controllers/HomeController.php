<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Circle;
use App\Triangle;

class HomeController extends Controller
{
    public function index(Request $request)
    {

		if ($request->isMethod('post')) {
		    
		    $circle = new Circle($request->circleRadius);
		    $triangle = new Triangle($request->triangleA, $request->triangleB, $request->triangleC);

		    $sumSurfaces = $this->sumSurfaces($circle, $triangle);
		    $sumCircumference = $this->sumCircumference($circle, $triangle);

		}
		else
		{
			$circle = '';
			$triangle = '';
			$sumCircumference = '';
			$sumSurfaces = '';
		}

        return view('index', compact('circle', 'triangle', 'sumCircumference', 'sumSurfaces'));
    }

    public function sumSurfaces($object1, $object2)
    {
    	return $object1->surface + $object2->surface;
    }

    public function sumCircumference($object1, $object2)
    {
    	return $object1->circumference + $object2->circumference;
    }

}
