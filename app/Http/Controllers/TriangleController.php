<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\Triangle as TriangleResource;
use App\Triangle;

class TriangleController extends Controller
{
    public function json($a, $b, $c)
    {
	    $triangle = new Triangle($a, $b, $c);

	    if($triangle->isValid)
	    {
	    	return response()->json(new TriangleResource($triangle));
	    }
	    else
	    {
	    	return "Not a valid triangle";
	    }
    }
}
