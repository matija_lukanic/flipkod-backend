<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Circle extends Model
{
    public $type = 'circle';
    public $radius;
    public $surface;
    public $circumference;

    public function __construct($radius)
    {
    	$this->radius = $radius;
    	$this->surface = $this->getSurface($radius);
    	$this->circumference = $this->getCircumference($radius);    	
    }

    public function getSurface($radius)
    {
    	//pi - 3.1415926535898
    	$surface = pi() * $radius * $radius;

    	return round($surface, 2);
    }

    public function getCircumference($radius)
    {
    	//pi - 3.1415926535898
    	$circumference = 2 * $radius * pi();
    	
    	return round($circumference, 2);
    }

}
