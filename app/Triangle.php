<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Triangle extends Model
{
    public $type = 'triangle';
    public $isValid = true;
    public $a;
    public $b;
    public $c;
    public $surface;
    public $circumference;

    public function __construct($a, $b, $c)
    {
    	//check if triangle is valid

    	if($this->ifValidTriangle($a, $b, $c))
    	{
    		//valid triangle

	    	$this->a = $a;
	    	$this->b = $b;
	    	$this->c = $c;
	    	$this->surface = $this->getSurface($a, $b, $c);
	    	$this->circumference = $this->getCircumference($a, $b, $c);    		
    	}
    	else
    	{
    		//not valid triangle
    		
    		$this->isValid = false;
    	}

    }

    public function getSurface($a, $b, $c)
    {
    	//get triangle surface if lenghts of all three triangle sides are known

    	$s = ($a + $b + $c) / 2;
    	$surface = sqrt( $s * ($s - $a) * ($s - $b) * ($s - $c));

        return round($surface, 2);
    }

    public function getCircumference($a, $b, $c)
    {
    	$circumference = $a + $b + $c;
        
        return round($circumference, 2);
    }

    public function ifValidTriangle($a, $b, $c)
    {
    	//a triangle is valid if sum of its two sides is greater than the third side
    	//these three conditions should be met

    	$check = ($a + $b > $c) && ($a + $c > $b) && ($b + $c > $a) ? true :  false;

    	return $check;
    }

}
